const express = require("express");
const morgan = require("morgan");

const app = express();

app.use(morgan("dev"));

app.get("/users", (req, res) => {
  res.send([
    {
      name: "user1",
      age: 22,
    },
    {
      name: "user2",
      age: 30,
    },
    {
      name: "user3",
      age: 18,
    },
    {
      name: "user4",
      age: 25,
    },
    {
      name: "user5",
      age: 10,
    },
  ]);
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log("app running on port 3000");
});
